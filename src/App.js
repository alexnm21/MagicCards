import React from 'react';
import { BrowserRouter } from "react-router-dom";
import Main from './Componentes/Main';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import appReducer from './reducers/appReducer';

class App extends React.Component {

  render() {

    const store = createStore(appReducer);

    return (
      <Provider store={store}> 
        <BrowserRouter>
          <Main/>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;