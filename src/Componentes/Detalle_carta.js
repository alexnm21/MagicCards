import React, { Component } from 'react';
import AccesoAPI from "../Modelo/AccesoApi";
import "./Detalle_carta.css";
import Cargando from './Globales/Cargando';
import { Link } from 'react-router-dom';
import { Compara_KEY, USER_KEY, FAV_KEY } from '../conf/const';
import DropdownMazos from './DropdownMazos';

import { connect } from 'react-redux';


class Detalle_carta extends Component {
    constructor(props) {
        super(props);

        this.state = {
            carta: {},
            recibido: false,
            idsCartasEnComparar: [],
            idsCartasEnFavoritos: []
        }

        this.Add_comparar = this.Add_comparar.bind(this);

    }

    componentDidMount() {
        this.verCarta();
        this._cargarListaCartasEnComparar();
        this._cargarListaCartasEnFavoritos();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.recarga !== this.props.recarga) {
            this._cargarListaCartasEnComparar();
            this._cargarListaCartasEnFavoritos();
        }
    }

    _recargarSidebar = () => {
        this.props.dispatch({
            type: 'RECARGA',
            item: !this.state.recarga,
        });
    }

    _cargarListaCartasEnComparar = () => {
        let allCartasEnComparar = JSON.parse(localStorage.getItem(Compara_KEY));
        let usuarioId = this._getUsuarioId();

        if (allCartasEnComparar !== null) {
            let [cartasEnComparar] = allCartasEnComparar.filter(el => el.idUsuario === usuarioId).map(el => el.carta);
            let idCartasEnComparar = []

            if (cartasEnComparar.length > 0) {
                idCartasEnComparar = cartasEnComparar.map(el => el.id);
            }

            this.setState({ idsCartasEnComparar: idCartasEnComparar });
        }
    }

    _cargarListaCartasEnFavoritos = () => {
        let allCartasEnFavoritos = JSON.parse(localStorage.getItem(FAV_KEY));
        let usuarioId = this._getUsuarioId();

        if (allCartasEnFavoritos !== null) {
            let cartasEnFavoritos = allCartasEnFavoritos.filter(el => el.idUsuario === usuarioId).map(el => el.carta);
            let idCartasEnFavoritos = [];

            if (cartasEnFavoritos[0] !== undefined) {
                idCartasEnFavoritos = cartasEnFavoritos[0].map(el => el.id);
            }

            this.setState({ idsCartasEnFavoritos: idCartasEnFavoritos });
        }
    }


    verCarta() {
        const id = this.props.match.params.id;




        AccesoAPI.leerApi("cards/" + id)
            .then(result => {
                this.setState({ carta: result.card, recibido: true })


            })



    }

    _getUsuario = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) return usuario.id;
        else return null;
    }


    Add_comparar = () => {
        let { carta } = this.state;
        let cartas_omparar = [];
        cartas_omparar = JSON.parse(localStorage.getItem(Compara_KEY));
        let userId = this._getUsuario();
        let usuarioNoExiste = true;


        if (localStorage.getItem(Compara_KEY) !== null) {
            cartas_omparar.forEach(el => {
                if (userId === el.idUsuario) {
                    usuarioNoExiste = false;

                    if (el.carta.filter(el => el.id === carta.id).length > 0) {


                        el.carta.forEach((cart, indice) => {
                            if (cart.id === carta.id) {
                                el.carta.splice(indice, 1);
                                localStorage.setItem(Compara_KEY, JSON.stringify(cartas_omparar));
                                this._recargarSidebar();
                                this._cargarListaCartasEnComparar();
                            }
                        })


                    }
                    else {

                        el.carta.push(carta);

                        localStorage.setItem(Compara_KEY, JSON.stringify(cartas_omparar));
                        this._recargarSidebar();
                        this._cargarListaCartasEnComparar();
                    }
                }
            })
            if (usuarioNoExiste) {
                let nuevaCard = { idUsuario: userId, carta: [carta] };
                cartas_omparar.push(nuevaCard);
                localStorage.setItem(Compara_KEY, JSON.stringify(cartas_omparar));
                this._recargarSidebar();
                this._cargarListaCartasEnComparar();
            }
        } else {
            let nuevaCarta_Comp = [{ idUsuario: userId, carta: [carta] }];
            localStorage.setItem(Compara_KEY, JSON.stringify(nuevaCarta_Comp));
            this._recargarSidebar();
            this._cargarListaCartasEnComparar();

        }
    }

    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) {
            return usuario.id
        }

        return null;
    }

    _addToFav = (card) => {
        let favoritos = [];
        favoritos = JSON.parse(localStorage.getItem(FAV_KEY));
        let userId = this._getUsuarioId();
        let usuarioNoExiste = true;

        //Esto sería como decir que: "Si el FAV_Key ya ha sido creado entonces haz..." Así que en su interior
        //Tiene que haber lo de añadir al array de favoritos del usuario que coge previamente la carta. El objeto

        if (localStorage.getItem(FAV_KEY) !== null) {
            favoritos.forEach((el, index) => {
                if (userId === el.idUsuario) {
                    usuarioNoExiste = false;
                    if (el.carta.filter(el => el.id === card.id).length > 0) {


                        el.carta.forEach((carta, indice) => {
                            if (carta.id === card.id) {
                                el.carta.splice(indice, 1);
                                localStorage.setItem(FAV_KEY, JSON.stringify(favoritos));
                                this._recargarSidebar();
                                this._cargarListaCartasEnFavoritos();
                            }
                        })

                    }
                    else {
                        el.carta.push(card); //Dentro del FKey que ya existe, dentro de "Carta", le introduzco la siguiente "card"
                        localStorage.setItem(FAV_KEY, JSON.stringify(favoritos));
                        this._recargarSidebar();
                        this._cargarListaCartasEnFavoritos();
                    }
                }
            })
            if (usuarioNoExiste) {
                let nuevaCard = { idUsuario: userId, carta: [card] };
                favoritos.push(nuevaCard);
                localStorage.setItem(FAV_KEY, JSON.stringify(favoritos));
                this._recargarSidebar();
                this._cargarListaCartasEnFavoritos();
            }
        }
        else {
            let nuevaCarta_Fav = [{ idUsuario: userId, carta: [card] }];
            localStorage.setItem(FAV_KEY, JSON.stringify(nuevaCarta_Fav));
            this._recargarSidebar();
            this._cargarListaCartasEnFavoritos();

        }
    }


    render() {

        let { carta } = this.state;


        if (!this.state.recibido) return <Cargando />;
        let spanish = true;

        let cartaEnComparar = this.state.idsCartasEnComparar.includes(carta.id);
        let cartaEnFavoritos = this.state.idsCartasEnFavoritos.includes(carta.id);



        if (typeof carta.name !== "undefined" && carta.foreignNames.length === 0) spanish = false;

        return (
            <div className="detalle-carta p-4">
                <div className="row">
                    <div className="col-4 d-flex justify-content-center">
                        <div>
                            {carta.imageUrl === undefined
                                ? <img className="img-fluid" alt={carta.name} src="https://i.ibb.co/cNjQCSV/carta-Por-Defecto3.png" />
                                : <img className="img-fluid" alt={carta.name} src={carta.imageUrl} />
                            }
                        </div>
                    </div>
                    <div className="col-6 pl-4">
                        <div>
                            <h4>Nombre de la carta</h4>
                            <p>{spanish ? carta.foreignNames[1].name : carta.name}</p>
                            <h4>Mazo</h4>
                            <p>{carta.setName}</p>
                            <h4>Tipo</h4>
                            <p>{carta.type}</p>
                        </div>
                        <div className="row">
                            <div className="col">
                                <span className="h5">Coste:</span>
                                <span className="ml-2"> {carta.manaCost}</span>
                            </div>
                            {carta.power !== undefined
                                ?
                                <div className="col">
                                    <span className="h5">Fuerza:</span>
                                    <span className="ml-2">{carta.power}</span>
                                </div>
                                : ""
                            }
                            {carta.toughness !== undefined
                                ?
                                <div className="col">
                                    <span className="h5">Resistencia:</span>
                                    <span className="ml-2">{carta.toughness}</span>
                                </div>
                                : ""
                            }

                        </div>
                        <div className="mt-3">
                            <h4>Efectos</h4>
                            <p>{spanish ? carta.foreignNames[1].text : carta.text}</p>
                        </div>





                    </div>
                </div>
                <div className="mt-5">
                    <div className="d-flex justify-content-between">
                        <div className="row testDiv">
                            <span className="btn btn-primary boton-detalles" onClick={() => this._addToFav(carta)}><i className={cartaEnFavoritos ? "fa fa-heart color-icons-selected" : "fa fa-heart-o"}></i></span>
                            <span className="btn btn-primary ml-1 boton-detalles" onClick={() => this.Add_comparar()}><i className={cartaEnComparar ? "fa fa-balance-scale color-icons-selected" : "fa fa-balance-scale"}></i></span>
                            <DropdownMazos styleClass="btn btn-primary boton-detalles" cartaActual={carta} />
                        </div>
                        <div>
                            <Link to={`/cards/${carta.set}`} className="btn btn-danger">Volver</Link>
                        </div>
                    </div>

                </div>
            </div>

        );
    }
}


const mapStateToProps = (state) => {
    return {
        recarga: state.recarga
    }
}

export default connect(mapStateToProps)(Detalle_carta);