import React from 'react';
import { USER_KEY } from "../../conf/const";
import { Redirect } from 'react-router-dom';
import { USUARIOS } from '../../datos/usuarios';
import { home } from '../../conf/routes';
import './Login.css';

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            usuario: '',
            password: '',
            errors: {},
            errorsAlert: '',
            initial: true
        }
    }

    _handleInputChange = (e) => {
        let target = e.target;

        let id = target.id;
        let value = target.value;

        this.setState({
            [id]: value
        }, this._handleValidation);
    }

    _handleValidation = (clickEnSubmit) => {
        let { usuario, password } = this.state;
        let errors = {};
        let valid = true;

        if (usuario.trim() === "") {
            valid = false;
            errors["usuario"] = "El campo no puede estar vacio.";
        }

        if (password.trim() === "") {
            valid = false;
            errors["password"] = "El campo no puede estar vacio.";
        }

        if (this.state.initial && !clickEnSubmit) errors = {};
        this.setState({ errors: errors });

        return valid;

    }

    _handleSubmit = (e) => {
        e.preventDefault();
        if (this._handleValidation(true)) {
            this._acceso();
        } else {
            this.setState({ initial: false })
        }

    }

    _acceso = () => {
        let usuarios = USUARIOS.filter(usu => usu.user === this.state.usuario && usu.password === this.state.password);

        if (usuarios.length > 0) {
            localStorage.setItem(USER_KEY, JSON.stringify(usuarios[0]));
            this.props.logged(true);
            this.setState({ usuario: '', password: '' });
        } else {
            this.setState({ errorsAlert: "Usuario o contraseña incorrecta" });
        }

    }

    _mostrarAlertError = () => {
        return (
            <div>
                <div className="alert alert-danger mb-4 text-center p-2">{this.state.errorsAlert}</div>
            </div>
        );
    }

    render() {

        if (localStorage.getItem(USER_KEY) !== null) return <Redirect to={home()} />

        return (
            <div className="container">
                <div className="row justify-content-md-center">
                    <div className="login-box col-4 d-flex flex-column align-self-center">
                        <div>
                            <h1>Login</h1>
                        </div>
                        {this.state.errorsAlert !== "" ? this._mostrarAlertError() : null}
                        <form onSubmit={this._handleSubmit}>
                            <div className="centered">
                                <div className="group">
                                    <input className="login-input" type="text" id="usuario" onChange={this._handleInputChange} value={this.state.usuario} required />
                                    <label className="login-label" htmlFor="usuario">Nombre de usuario</label>
                                </div>
                            </div>
                            <small className="form-text text-danger">{this.state.errors["usuario"]}</small>

                            <div className="centered">
                                <div className="group">
                                    <input className="login-input" type="password" id="password" onChange={this._handleInputChange} value={this.state.password} required />
                                    <label className="login-label" htmlFor="password">Contraseña</label>
                                </div>
                            </div>
                            <small className="form-text text-danger">{this.state.errors["password"]}</small>

                            <button className="boton-inicio" type="submit">INICIAR SESION</button>

                        </form>
                        {/* <div className="text-center">
                            <Link className="link-text" to="#">No estoy registrado</Link>
                        </div> */}
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;