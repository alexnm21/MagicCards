import React from 'react';
import '../globales/estilos.css';
import './Menu.css';
import { sets, comparar, home } from '../conf/routes';
import { Link } from 'react-router-dom';

import { USER_KEY } from '../conf/const';

class Menu extends React.Component {


  render() {

    //LLAMADA A USER_KEY DEL LOCAL STAGE
    let getUserKey = localStorage.getItem(USER_KEY);
    if (getUserKey === null) return "";
    let getUserKeyValue = JSON.parse(getUserKey);


    return (
      <>
        <nav className="navbar navbar-expand navbar-color mr-0 pl-4">
        <Link style={{height: '100%'}} to={home()} >
          <img className="navbar-brand img-fluid" alt="Logo Magic Cards" style={{height: '100%'}} src={process.env.PUBLIC_URL + '/img/logo-magic.png'} />  
        </Link>
          <div className="ml-4" style={{width: '100%', height: '100%'}}>
            <ul className="navbar-nav" style={{height: '100%'}}>
              <li className="nav-item item-nav-custom">
                <Link className="nav-link link-nav-custom" to={sets()} >Mazos</Link>
              </li>
              <li className="nav-item item-nav-custom">
                <Link className="nav-link link-nav-custom" to={comparar()} >Comparar</Link>
              </li>
            </ul>
          </div>

          <div className="mr-3">
            <div className="d-flex">
              <div>
                <div className="dropdown dropdown-perfil">
                  <button className="btn dropdown-toggle dropdown-perfil" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {getUserKeyValue.name}
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <span className="dropdown-item" style={{ cursor: 'pointer' }} href="#" onClick={() => this.props.logged(false)}>Cerrar session</span>
                  </div>
                </div>
              </div>

              <div>
                <div className="foto-perfil">
                  <img src={getUserKeyValue.imgperfil} alt={getUserKeyValue.name} className="img-fluid" />
                </div>
              </div>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default Menu;