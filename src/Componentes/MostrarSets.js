import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AccesoApi from '../Modelo/AccesoApi';
import Cargando from './Globales/Cargando';

export default class MostrarSets extends Component {

    constructor(props) {
        super(props);

        this.state = {
            recibidoSets: false,
            sets: []
        }

    }

    componentDidMount() {
        this._chargeAPI();
    }

    _chargeAPI = () => {
        AccesoApi.leerApi(`sets/`)
            .then(response => {
                this.setState(response);
                this.setState({ recibidoSets: true });
            })
            .catch(error => console.log(error));
    }

    _mensajeCargandoDatos = () => {

        return (
            <tr>
                <td colSpan="3">
                    <Cargando />
                </td>
            </tr>
        );

    }

    _crearFilas = () => {
        return (
            this.state.sets.map((set, index) => {
                return (
                    <tr key={index}>
                        <td><Link to={`/cards/${set.code}`}>{set.name}</Link></td>
                        <td>{set.code}</td>
                        <td>{set.type}</td>
                    </tr>
                );
            })
        );
    }

    render() {
     
        if(!this.state.recibidoSets) return <Cargando />
      
        return (
            <div>
                <table className="table table-dark table-hover table-sm">
                    <thead>
                        <tr>
                            <th>Mazos</th>
                            <th>Code</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this._crearFilas()} 
                    </tbody>
                </table>
            </div>
        );
    }
}