import React, { Component } from "react";
import Chevron from "./Chevron";

import "./Acordion.css";


class Acordion extends Component {
    constructor(props) {
        super(props);

        this.state = {
            setActive: false,
            setHeight: "0px",
            setRotation: false


        }
    }


    _toggleClass = () => {
        const currentState = this.state.setActive;
        this.setState({ setActive: !currentState });


        if (!currentState) {
            this.setState({ setHeight: "none" });
        } else if (currentState) {
            this.setState({ setHeight: "0px" });
        }


        const currentRotation = this.state.setRotation;
        this.setState({ setRotation: !currentRotation });


        // const currentContent = this.state.contentRef;

    }


    render() {
        return (
            <div className="accordion__section">
                <button className={`accordion ${this.state.setActive ? "active" : ""}`} onClick={this._toggleClass}>
                    <div className="row">
                        <p className="col-auto accordion__title">{this.props.title}</p>
                        <Chevron className={`col-auto accordion__item ${this.state.setRotation ? "rotate" : ""}`} width={10} fill={"#777"} />
                    </div>
                </button>

                <div style={{ maxHeight: `${this.state.setHeight}` }} className="accordion__content">
                    <div className="accordion__text">
                        {this.props.content===undefined
                            ? this.props.children
                            : this.props.content
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Acordion;