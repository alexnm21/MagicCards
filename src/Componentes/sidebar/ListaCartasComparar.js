import React from 'react';
import { Compara_KEY, USER_KEY } from '../../conf/const';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { comparar } from '../../conf/routes';
import { Link } from 'react-router-dom';
import './ListaCartasComparar.css';
import { connect } from 'react-redux';

class ListaCartasComparar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            cartas: [],
            recarga: false
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.recarga !== this.props.recarga) {
            this._cargaComponente();
        }
    }

    componentDidMount() {
        this._cargaComponente();
    }

    _recargarPrincipal = () => {
        this.props.dispatch({
            type: 'RECARGA',
            item: !this.state.recarga,
        });
    }

    _cargaComponente = () => {
        let idUsuario = this._getUsuarioId();
        this._getCartas(idUsuario);
    }

    _getCartas = (idUsuario) => {
        let cartas = JSON.parse(localStorage.getItem(Compara_KEY));

        if (cartas !== null) {
            let cartasUsuario = cartas.filter(carta => carta.idUsuario === idUsuario).map(el => el.carta);
            cartasUsuario.length > 0 ? this.setState({ cartas: cartasUsuario[0] }) : this.setState({ mazos: cartasUsuario })
        }
    }

    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) return usuario.id;
        else return null;
    }

    _cartasComparar = () => {
        return (
            <div className="d-flex justify-content-end p-3 mb-3" style={{ borderBottom: '1px solid #505050' }}>
                <Link to={comparar()} className="btn btn-info">Comparar</Link>
            </div>
        );
    }

    _eliminarCarta = (idCarta) => {
        let idUser = this._getUsuarioId();
        let comparaEntero = JSON.parse(localStorage.getItem(Compara_KEY));

        comparaEntero.forEach(elemento => {
            if (idUser === elemento.idUsuario) {
                if (elemento.carta.filter(el => el.id === idCarta).length > 0) {
                    elemento.carta.forEach((carta, indice) => {
                        if (carta.id === idCarta) {
                            elemento.carta.splice(indice, 1);
                            localStorage.setItem(Compara_KEY, JSON.stringify(comparaEntero));
                            this.setState({ cartas: elemento.carta, visible: false }, this._recargarPrincipal());
                        }
                    })
                }
            }
        })
    }

    _renderCartas = (carta, index) => {
        return (
            <div key={index}>
                <Draggable draggableId={index + ''} index={index}>
                    {(provided, snapshot) => (

                        <div className={index <= 1 ? "opcion_cartas" : "opcion_cartas cartas_disabled"} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                            <div className="row">
                                <div className="col-10">
                                    {carta.imageUrl !== undefined
                                        ? <img alt="imagen de la carta" className="img-fluid mr-2 img-card" src={carta.imageUrl} />
                                        : <img alt="imagen generica" className="img-fluid mr-2 img-card" src="https://i.ibb.co/cNjQCSV/carta-Por-Defecto3.png" />
                                    }
                                    <span>{carta.name}</span>
                                </div>
                                <div className="col-2">
                                    <button onClick={() => this._eliminarCarta(carta.id)} className="btn" title="Borrar Carta"><i className="fa fa-times iconoTrash"></i></button>
                                </div>
                            </div>
                        </div>

                    )}
                </Draggable>
                {index === 1
                    ? this._cartasComparar()
                    : null
                }
            </div>
        );
    }

    onDragEnd = (result) => {
        const { destination, source, reason } = result;
        if (!destination || reason === 'CANCEL') {
            return;
        }
        if (!destination.droppableId === source.droppableId && destination.index === source.index) {
            return;
        }

        const cartas = Object.assign([], this.state.cartas);
        const droppedCarta = this.state.cartas[source.index];

        //imagina que tenemos este array [1,2,3,4]
        cartas.splice(source.index, 1); //[1,2,4] el 3 lo hemos quitado pero lo tenemos en el dropped
        cartas.splice(destination.index, 0, droppedCarta);//[droppedCarta, 1,2,4] Donde dropped es igual a 3


        let storage = JSON.parse(localStorage.getItem(Compara_KEY));
        let idUsuario = this._getUsuarioId();
        storage.forEach(el => {
            if (el.idUsuario === idUsuario) {
                el.carta.splice(0);
                el.carta.push(...cartas);
                localStorage.setItem(Compara_KEY, JSON.stringify(storage));
            }
        });

        this.setState({
            cartas: cartas
        })
    }

    render() {

        if (this.state.cartas.length === 0) return <h6 className="text-center font-weight-bold">No hay cartas para mostrar</h6>;

        return (
            <>
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <div>
                        <Droppable droppableId='CARTAS_COMPARAR'>
                            {(provided, snapshot) => (
                                <div ref={provided.innerRef} {...provided.droppableProps} className="lista">{/* La clase lista no hace nada solo es informativa (de momento...) */}
                                    {this.state.cartas.map(this._renderCartas)}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </div>
                </DragDropContext>
            </>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        recarga: state.recarga
    }
}

export default connect(mapStateToProps)(ListaCartasComparar);