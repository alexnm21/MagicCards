import React from 'react';
import OpcionMazosSidebar from './OpcionMazosSidebar';
import FormularioCrearMazo from './FormularioCrearMazo';
import { MAZO_KEY, USER_KEY } from '../../conf/const';
import { Redirect } from 'react-router-dom';
import "./OpcionMazosSidebar.css";


class MazosSidebar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            mazos: [],
            visible: false,
            redirect: null
        }


    }

    componentDidMount() {
        let idUsuario = this._getUsuarioId();
        this._getMazos(idUsuario);
        
    }

    _getMazos = (idUsuario) => {
        let mazokey = JSON.parse(localStorage.getItem(MAZO_KEY));

        if (mazokey !== null) {
            let mazos = mazokey.filter(mazo => mazo.idUsuario === idUsuario).map(el => el.mazos);
            mazos.length > 0 ? this.setState({ mazos: mazos[0] }) : this.setState({ mazos: mazos })
        }
    }
    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) return usuario.id;
        else return null;
    }

    _nuevoMazo = (mazo) => {
        let mazos = this.state.mazos;
        mazos.push(mazo);
        this.setState({
            mazos: mazos
        })
    }

    _show = () => {
        this.setState({ visible: true });
    }

    _hide = () => {
        this.setState({ visible: false });
    }

    _eliminarMazo = (idMazo) => {
        let idUser = this._getUsuarioId();
        // console.log(idMazo);
        let mazoEntero = JSON.parse(localStorage.getItem(MAZO_KEY));
        // console.log(mazoEntero);

        mazoEntero.forEach(elemento => {
            if (idUser === elemento.idUsuario) {
                // console.log(elemento);
                if (elemento.mazos.filter(el => el.id === idMazo).length > 0) {
                    elemento.mazos.forEach((mazo, indice) => {
                        // console.log(mazo);
                        if (mazo.id === idMazo) {
                            // console.log(mazo)
                            elemento.mazos.splice(indice, 1);
                            // console.log(elemento.mazos);
                            localStorage.setItem(MAZO_KEY, JSON.stringify(mazoEntero));

                            this.setState({ mazos: elemento.mazos, visible: false, redirect: window.location.pathname});
                        }
                    })
                }
            }
        })

    }

    render() {

        return (
            <>
                {this.state.redirect!==null ?  <Redirect to={this.state.redirect} /> :  null}
                
                {
                    this.state.mazos.length > 0
                        ? this.state.mazos.map((mazo, index) => {
                            return (
                                <div key={index}>
                                    <OpcionMazosSidebar remove={this._eliminarMazo} visible={this.state.visible} show={() => this._show()} hide={() => this._hide()} >
                                        {mazo}
                                    </OpcionMazosSidebar>
                                </div>
                            );
                        })
                        : <h6 className="text-center font-weight-bold mb-4">No hay mazos para mostrar</h6>
                }

                <FormularioCrearMazo nuevo_mazo={this._nuevoMazo} />

            </>
        );
    }
}

export default MazosSidebar;