export function login() {
    return "/";
}
export function home() {
    return "/home";
}
export function sidebar() {
    return "/";
}
export function logout() {
    return "/logout";
}

export function sets() {
    return "/sets";
}

export function cards() {
    return "/cards/:code";
}

export function cardById() {
    return "/card/:id";
}
export function comparar() {
    return "/comparar";
}

export function mazoById() {
    return "/mazo/:idMazo";
}