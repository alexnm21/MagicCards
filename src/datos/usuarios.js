const USUARIOS = [
    {
        user: "usuario1",
        password: "usu1",
        id: "01",
        name: "Usuario Uno",
        imgperfil: "https://images.unsplash.com/photo-1521572267360-ee0c2909d518?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
    },
    {
        user: "usuario2",
        password: "usu2",
        id: "02",
        name: "Usuario Dos",
        imgperfil: "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80" //imagen de prueba luego las cambio, Alex no te ralles
    },
    {
        user: "usuario3",
        password: "usu3",
        id: "03",
        name: "Usuario Tres",
        imgperfil: "https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80"
    }
]

export {USUARIOS};